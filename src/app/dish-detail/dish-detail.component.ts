import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, FormGroup, Validators, Form } from '@angular/forms';

import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import { DISHES } from '../shared/dishes';

import { switchMap } from 'rxjs/operators';



@Component({
  selector: 'app-dish-detail',
  templateUrl: './dish-detail.component.html',
  styleUrls: ['./dish-detail.component.scss']
})
export class DishDetailComponent implements OnInit {

  dish: Dish;
  errMess: string;
  dishIds: string[];
  prev: string;
  next: string;
  rating: number;

  displayPreview = false;

  newCommentForm: FormGroup;
  @ViewChild('fform') newCommentFormDirective;

  formErrors =  {
    name: '',
    comment: ''
  };

  validationMessages = {
    name: {
      'required': 'Name is required.',
      'minlength': 'Name must be at least 2 characters long.',
      'maxlength': 'Name must cannot exceed 25 characters long.'
    },
    comment: {
      'required': 'Comment is required.',
      'minlength': 'Comment must be at least 15 characters long.',
      'maxlength': 'Comment must cannot exceed 200 characters long.'
    }
  };

  ngOnInit() {
    this.dishService.getDishIds()
    .subscribe( (dishIds) => this.dishIds = dishIds);

    this.route.params
    .pipe(switchMap((params: Params) => this.dishService.getDish(params['id'])))
    .subscribe(dish => { this.dish = dish; this.setPrevNext(dish.id); },
         errmess => this.errMess = <any>errmess);
  }

  constructor(private dishService: DishService, 
        private route: ActivatedRoute,
        private location: Location,
        private fb: FormBuilder,
        @Inject('BaseURL') private BaseURL) { 

        this.createForm();

        }

  createForm() {
    this.newCommentForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)] ],
      comment: ['', [Validators.required, Validators.minLength(15), Validators.maxLength(200)] ]
    });

    this.newCommentForm.valueChanges
    .subscribe(data => this.onValueChanged(data));

    this.onValueChanged(); 

  }

onValueChanged(data?: any) {
  if (!this.newCommentForm) { return; }
  const form = this.newCommentForm;
  for (const field in this.formErrors) {
    if (this.formErrors.hasOwnProperty(field)) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          if (control.errors.hasOwnProperty(key)) {
            this.formErrors[field] += messages[key] + ' ';
          }
        }
      }
    }
  }

    if(this.newCommentForm.valid) {
      this.displayPreview = true;
    } else {
      this.displayPreview = false;
    }

}

pitch(event: any) {
  this.rating = event.value;
}

onSubmit() {
    var newComment = {
      rating: this.rating,
      comment: this.newCommentForm.getRawValue().comment,
      author: this.newCommentForm.getRawValue().name,
      date: (new Date()).toString()
    };

    DISHES[this.dish.id].comments.push(newComment);

    this.newCommentFormDirective.resetForm();
}

  setPrevNext(dishId: string) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length]
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length]
  }

  goBack(): void {
    this.location.back();
  }

}
